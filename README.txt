Manual Re-Alignment of Keystrokes to Tokens. In some cases, keystrokes are not mapped to the correct token. The manual re-alignment procedure below helps rectify this issue:

NOTE: You will have to install the Python library "lxml" for the scripts to work.

1) Download the following repository from GitLab: https://git.rwth-aachen.de/arndt.heilmann/kdfixer.
2) Download the Event.xml-files from your study from the Management Tool: https://critt.as.kent.edu/cgi-bin/yawat/yawat.cgi. 
3) Move the Event.xml-files to the folder "EventFiles".
4) Run "1_CreatePuzzleFileFromEventFile.py" from the Scripts folder.
5) In the Folder "ManualRealignment" you will find so called .pzl files. These are basically tab-separated tables.
6) Open the .pzl-File in a spreadsheet program (or copy-paste the content of a .pzl-file into the spreadsheet program).
7) Cut-and-paste misaligned keystroke ids to the correct token id.
8) Save your changes under the .pzl-file again (tab-separated).
9) Run "2_CreateFixedEventFile".
10) Retrieve updated .Event.xml-Files from "FixedEventFiles" Folder.
11) Upload  .Event.xml-files via the CRITT TPRDB management tool and re-create the tables.
12) The tool was developed as part of the TRICKLET-project.